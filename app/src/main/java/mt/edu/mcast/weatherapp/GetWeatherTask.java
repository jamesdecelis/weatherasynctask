package mt.edu.mcast.weatherapp;

import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;

public class GetWeatherTask extends AsyncTask<String, Void, Double> {

    @Override
    protected Double doInBackground(String... location) {

        double temp = 0;

        WeatherHttpClient client = new WeatherHttpClient();

        String resultJSON = client.getWeatherData(location[0]);

        JSONObject jObj = null;
        try {
            jObj = new JSONObject(resultJSON);

            JSONObject mainObj = jObj.getJSONObject("main");
            temp = mainObj.getDouble("temp");

    } catch (JSONException e) {
        e.printStackTrace();
    }
        return temp;
    }
}
