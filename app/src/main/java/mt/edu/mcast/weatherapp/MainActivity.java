package mt.edu.mcast.weatherapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void getWeather(View v){

        EditText etxtLocation = findViewById(R.id.etxtLocation);


        GetWeatherTask task = new GetWeatherTask();

        try {
            double temp = task.execute(etxtLocation.getText().toString()).get();

            TextView txtTemp = findViewById(R.id.txtv_temp);
            txtTemp.setText(String.valueOf(temp));


        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
